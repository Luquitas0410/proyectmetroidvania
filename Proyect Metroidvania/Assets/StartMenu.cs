using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class StartMenu : MonoBehaviour
{
    public AK.Wwise.Event buttom;
    public void Play()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Exit()
    {
        Debug.Log("Exit");
        Application.Quit(); 
    }

    public void Click()
    {
        buttom.Post(gameObject);
    }
}
