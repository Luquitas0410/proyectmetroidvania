using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AK.Wwise.Event PlayMusic;
    public AK.Wwise.Event StopMusic;
    public AK.Wwise.Event EnterZona1, EnterZona2, EnterZona3, EnterZona4, EnterZona5, EnterZona6, EnterZona7;
    public AK.Wwise.Event ExitZona1, ExitZona2, ExitZona3, ExitZona4, ExitZona5, ExitZona6, ExitZona7;
    void Start()
    {
        PlayMusic.Post(gameObject);
    }
    private void OnDestroy()
    {
        StopMusic.Post(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Zona1"))
            EnterZona1.Post(gameObject);
        if (other.gameObject.CompareTag("Zona2"))
            EnterZona2.Post(gameObject);
        if (other.gameObject.CompareTag("Zona3"))
            EnterZona3.Post(gameObject);
        if (other.gameObject.CompareTag("Zona4"))
            EnterZona4.Post(gameObject);
        if (other.gameObject.CompareTag("Zona5"))
            EnterZona5.Post(gameObject);
        if (other.gameObject.CompareTag("Zona6"))
            EnterZona6.Post(gameObject);
        if (other.gameObject.CompareTag("Zona7"))
            EnterZona7.Post(gameObject);

    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Zona1"))
            ExitZona1.Post(gameObject);
        if (other.gameObject.CompareTag("Zona2"))
            ExitZona2.Post(gameObject);
        if (other.gameObject.CompareTag("Zona3"))
            ExitZona3.Post(gameObject);
        if (other.gameObject.CompareTag("Zona4"))
            ExitZona4.Post(gameObject);
        if (other.gameObject.CompareTag("Zona5"))
            ExitZona5.Post(gameObject);
        if (other.gameObject.CompareTag("Zona6"))
            ExitZona6.Post(gameObject);
        if (other.gameObject.CompareTag("Zona7"))
            ExitZona7.Post(gameObject);

    }
  
}
