/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID MUTE_ZONA1 = 2376863836U;
        static const AkUniqueID MUTE_ZONA2 = 2376863839U;
        static const AkUniqueID MUTE_ZONA3 = 2376863838U;
        static const AkUniqueID MUTE_ZONA4 = 2376863833U;
        static const AkUniqueID MUTE_ZONA5 = 2376863832U;
        static const AkUniqueID MUTE_ZONA6 = 2376863835U;
        static const AkUniqueID MUTE_ZONA7 = 2376863834U;
        static const AkUniqueID PLAY_BOMB = 1323964334U;
        static const AkUniqueID PLAY_BUTTOM = 2099555729U;
        static const AkUniqueID PLAY_CC = 2346744684U;
        static const AkUniqueID PLAY_CIRCLE_ENEMY_DEATH = 956488832U;
        static const AkUniqueID PLAY_CIRCLE_ENEMY_OUCH = 153508967U;
        static const AkUniqueID PLAY_CP = 2346744703U;
        static const AkUniqueID PLAY_ENEMY_OUCH = 3209061548U;
        static const AkUniqueID PLAY_EX = 2313189265U;
        static const AkUniqueID PLAY_FIRE = 3015324718U;
        static const AkUniqueID PLAY_GAME_OVER = 725000905U;
        static const AkUniqueID PLAY_HANGED_ENEMY_DEATH = 3016419867U;
        static const AkUniqueID PLAY_HANGED_ENEMY_OUCH = 1389883578U;
        static const AkUniqueID PLAY_HP_UP = 2791035522U;
        static const AkUniqueID PLAY_ITEM = 698828439U;
        static const AkUniqueID PLAY_LAUGH = 2772719461U;
        static const AkUniqueID PLAY_MAGIC_ECO = 686539257U;
        static const AkUniqueID PLAY_MAGICEX = 2951541624U;
        static const AkUniqueID PLAY_MUSICA = 4237703276U;
        static const AkUniqueID PLAY_ONDA_MAGICA = 4016540981U;
        static const AkUniqueID PLAY_OUCH = 1979731779U;
        static const AkUniqueID PLAY_PAUSE = 4233560256U;
        static const AkUniqueID PLAY_SALTO = 2695653357U;
        static const AkUniqueID PLAY_SAW = 3225017965U;
        static const AkUniqueID PLAY_SWORD = 1191497377U;
        static const AkUniqueID PLAY_TICTAC = 4082191346U;
        static const AkUniqueID PLAY_TRAMPOLINE = 3799408143U;
        static const AkUniqueID PLAY_WIN = 2955987680U;
        static const AkUniqueID RESET_VOICE_VOLUME_MX1 = 3502641739U;
        static const AkUniqueID SET_VOICE_VOLUME_MX1 = 462605908U;
        static const AkUniqueID STOP_MUSICA = 3034517274U;
        static const AkUniqueID STOP_ONDA_MAGICA = 3384217499U;
        static const AkUniqueID STOP_PAUSE = 3298098222U;
        static const AkUniqueID UNMUTE_ZONA1 = 3955479313U;
        static const AkUniqueID UNMUTE_ZONA2 = 3955479314U;
        static const AkUniqueID UNMUTE_ZONA3 = 3955479315U;
        static const AkUniqueID UNMUTE_ZONA4 = 3955479316U;
        static const AkUniqueID UNMUTE_ZONA5 = 3955479317U;
        static const AkUniqueID UNMUTE_ZONA6 = 3955479318U;
        static const AkUniqueID UNMUTE_ZONA7 = 3955479319U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace ZONAS
        {
            static const AkUniqueID GROUP = 764656370U;

            namespace STATE
            {
                static const AkUniqueID BOSS = 1560169506U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID NORMAL = 1160234136U;
            } // namespace STATE
        } // namespace ZONAS

    } // namespace STATES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID METROIDVANIA = 3408196680U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
