using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public AK.Wwise.Event cp;
    Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (animator != null) { 
             animator.SetTrigger("Activate");
            cp.Post(gameObject);
        }
        CheckPointController.Instance.LastCP(gameObject);
        GameDataController.Instance.SaveData();
    }
}
