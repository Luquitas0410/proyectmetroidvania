using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlatforms : MonoBehaviour
{
    public Transform[] wayPoints;
    public float moveSpeed;
    private int nextPlatform = 1;
    private bool platformOrder = true;
    void Start()
    {
        
    }

    void Update()
    {
        if (platformOrder && nextPlatform + 1 >= wayPoints.Length)
        {
            platformOrder = false;
        }

        if (!platformOrder && nextPlatform <= 0)
        {
            platformOrder = true;
        }

        if (Vector2.Distance(transform.position, wayPoints[nextPlatform].position) < 0.1f)
        {
            if(platformOrder)
            {
                nextPlatform += 1;
            }
            else
            {
                nextPlatform -= 1;
            }
        }
        transform.position = Vector2.MoveTowards(transform.position, wayPoints[nextPlatform].position,moveSpeed * Time.deltaTime);

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
         {
            collision.transform.SetParent(this.transform);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.transform.SetParent(null);
        }
    }
}

    
