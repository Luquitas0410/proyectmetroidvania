using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CharlyCharly : MonoBehaviour
{
    public GameObject buttom, HPitem, Ex;
    private GameObject player;
    private Animator Animator;
    public float nextSpinTime, betweenSpinTime;

    public AK.Wwise.Event cc;
    public AK.Wwise.Event win;

    void Start()
    {
        player = GameObject.Find("Player");
        Animator = GetComponent<Animator>();
        HPitem.SetActive(false);
        buttom.SetActive(false);
    }

    void Update()
    {
        Vector3 distance = player.transform.position - this.transform.position;

        int x = Random.Range(0,2);

        if (distance.magnitude <=2 && buttom != null)
        {
            buttom.SetActive(true);

            if (nextSpinTime > 0)
            {
                nextSpinTime -= Time.deltaTime;
            }

            if (Input.GetKeyDown("e") && nextSpinTime <=0)
            {
                Destroy(buttom.gameObject);
                cc.Post(gameObject);
                if (x ==1)
                {
                    Animator.SetBool("Win", true);
                    Animator.SetBool("Lose", false);
                    Invoke("Win", 3);
                    win.Post(gameObject);
                }
                if (x ==0)
                {
                    Animator.SetBool("Lose", true);
                    Animator.SetBool("Win", false);
                    Invoke("Lose", 3);
                }
                nextSpinTime = betweenSpinTime;
            }
        }
        else if (buttom !=null)
            buttom.SetActive(false);
    }

    void Win()
    {
        HPitem.SetActive(true);
    }
    void Lose()
    {
        Instantiate(Ex, player.transform.position, Quaternion.identity);
    }

   
}
