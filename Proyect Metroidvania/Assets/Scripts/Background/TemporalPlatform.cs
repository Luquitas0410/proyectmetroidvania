using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporalPlatform : MonoBehaviour
{

    public float disapairTime;
    private Animator Animator;
    public float timer;
    public GameObject parent;
    public AK.Wwise.Event tictac;

    void Start()
    {
        Animator = GetComponent<Animator>();
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Animator.SetTrigger("Dis");
            tictac.Post(gameObject);
            Invoke("Despawn", disapairTime);
        }
    }


    private void Despawn()
    {
        this.gameObject.SetActive(false);
       parent.SetActive(false); 
        Invoke("Spawn", 5);
    }
    private void Spawn()
    {
        this.gameObject.SetActive(true);
        parent.SetActive(true);
    }
    
}
