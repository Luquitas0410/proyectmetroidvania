using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saw : MonoBehaviour
{
    public float speed;
    public AK.Wwise.Event saw;

    void Start()
    {
        saw.Post(gameObject);
    }

    // Update is called once per frame
    void Update()
    {

        this.transform.Rotate(0, 0, speed * Time.deltaTime);
       

    }
}
