using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;



public class GameManager : MonoBehaviour
{
    private Health health;
    void Start()
    {
        health = GetComponent<Health>();    
    }

    // Update is called once per frame
    void Update()
    {
       if (Input.GetKeyUp(KeyCode.Escape)) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }
    }
    private void OnTriggerEnter2D(Collider2D limit)
    {
        if (limit.gameObject.CompareTag("Reset") == true)
        {
            health.SaveHP();
            health.ReceiveDamage(new Vector2(1, 1));
            Invoke("SceneRestart", 0.2f);
        }
    }
    private void SceneRestart()
    {
        GameDataController.Instance.LoadData();
    }
}
