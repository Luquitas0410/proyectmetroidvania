using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckPointController : MonoBehaviour
{
    public static CheckPointController Instance;

    [SerializeField] private GameObject[] checkPoints;
    [SerializeField] private GameObject player, startPoint;
    private int cpIndex;
    void Awake()
    {
        Instance = this;
        checkPoints = GameObject.FindGameObjectsWithTag("CheckPoint");
        cpIndex = PlayerPrefs.GetInt("indexPoints"); 
        player.transform.position = checkPoints[cpIndex].transform.position ;
    }

    public void LastCP(GameObject checkPoint)
    {
        for (int i = 0; i < checkPoints.Length; i++)
        {
            if (checkPoints[i] == checkPoint)
            {
                PlayerPrefs.SetInt("indexPoints", i); 
            }
        }
    }
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            player.transform.position = startPoint.transform.position;
        }
    }
}
