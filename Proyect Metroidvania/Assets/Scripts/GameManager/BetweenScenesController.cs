using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.VisualScripting;

public class BetweenScenesController : MonoBehaviour
{
    public static BetweenScenesController Instance;

    public int deathCount;
    public TMPro.TMP_Text count;
    private GameObject player;
    private Dash dash;
    private PlayerAtack sword;
    static public bool haveDash;
    public bool haveDashPublic;

    static public bool haveSword;
    public bool haveSwordPublic;
    private void Awake()
    {
        if (BetweenScenesController.Instance == null)
        {
            BetweenScenesController.Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        
    }
    private void Start()
    {
        player = GameObject.Find("Player");
    }
    private void Update()
    {
        sword = FindObjectOfType<PlayerAtack>();
        dash = FindObjectOfType<Dash>();

        dash.enabled = haveDash;
        haveDashPublic = haveDash;

        sword.enabled = haveSword;
        haveSwordPublic = haveSword;
        /* if (haveDash == true)
         {
             EnabledDash();
         }
       */
    }
    public void AddDeath()
    {
        deathCount++;
        count.text = "" + deathCount.ToString("");
    }
  /* public void EnabledDash()
    {
            dash.enabled = true;
        Debug.Log("blu leibel");
    }*/
}