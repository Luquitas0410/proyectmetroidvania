using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UIElements;

public class GameDataController : MonoBehaviour
{
    public static GameDataController Instance;

    public Animator animator;
    private GameObject player;
    public string saveArchive;
    public GameData gameData = new GameData();
    public HUD hud;
    private Health health;
    private BetweenScenesController deaths;
    private void Awake()
    {
        Instance = this;
        saveArchive = Application.dataPath + "/gameData.json";
        player = GameObject.FindGameObjectWithTag("Player");
        health = new Health();
        
    }
    void Start()
    {
  
    }

    void Update()
    {
        /*
       if (Input.GetKeyDown(KeyCode.C)) 
        {
            LoadData();
        }
       if (Input.GetKeyDown(KeyCode.G))
        {
            SaveData();
        }*/
    }

    public void LoadData()
    {
        if (File.Exists(saveArchive))
        {
            string content = File.ReadAllText(saveArchive);
            gameData = JsonUtility.FromJson<GameData>(content);

            health.LoadHP();
            player.transform.position = gameData.positionData;
           // player.GetComponent<Health>().hp = gameData.hpData;//
        }
        else
            Debug.Log("No data");
    }
    public void SaveData()
    {
        health.SaveHP();
        animator.SetTrigger("SaveActivate");
        GameData newData = new GameData()
        {
            positionData = player.transform.position,
            //   hpData = player.GetComponent<Health>().hp,//

        };
    string JSONconvert = JsonUtility.ToJson(newData);   
        File.WriteAllText(saveArchive, JSONconvert);
        Debug.Log("Game saved");
        
    }
}
