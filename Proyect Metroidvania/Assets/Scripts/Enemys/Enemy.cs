using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int speed = 4;
    private GameObject player;
    private Animator Animator;
    [SerializeField] private float distDetection = 4;
    private EnemyHealth enemyHealth;
    public float nextAttackTime, betweenAttacksTime;


    public AK.Wwise.Event spawn;


    void Start()
    {
        enemyHealth = GetComponent<EnemyHealth>();
        player = GameObject.Find("Player");
        Animator = GetComponent<Animator>();
       
    }

    void Update()
    {
        Vector3 distance = player.transform.position - this.transform.position;

        if (distance.magnitude <= distDetection)
        {
            Invoke("Following", 1.2f);
            if (nextAttackTime > 0)
            {
                nextAttackTime -= Time.deltaTime;
            }
            if (nextAttackTime == 0)
            {
                distDetection = 10;
                Animator.SetBool("Spawn", true);
                spawn.Post(gameObject);
                
                nextAttackTime = betweenAttacksTime;
            }
           
        }
    }
  
        public IEnumerator Stop()
        {
            speed = 0;
            yield return new WaitForSeconds(0.5f);
            speed = 4;
        }

     
    private void Following()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, player.transform.position, speed * Time.deltaTime);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, distDetection);
    }
}
