using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombEnemy : MonoBehaviour
{
    public int speed;
    private GameObject player;
    private Animator Animator;
    public GameObject Ex;
    private float distDetection;
    private EnemyHealth enemyHealth;
 
    void Start()
    {
        player = GameObject.Find("Player");
        Animator = GetComponent<Animator>();
        distDetection = 4;
        enemyHealth = GetComponent<EnemyHealth>();
    }
    void Update()
    {
        Vector3 distance = player.transform.position - this.transform.position;

        if (distance.magnitude <= distDetection)
        {
            distDetection = 8;
            Animator.SetBool("Spawn", true);
            Invoke("Following", 0.6f);
            Invoke("Explotion", 5);
          
        }
    }
    private void Following()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, player.transform.position, speed * Time.deltaTime);
    
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
            if (other.gameObject.CompareTag("Player"))
              Explotion();    
    }

    public void Explotion()
    {
        Destroy(this.gameObject);
        Instantiate(Ex, this.transform.position, Quaternion.identity);
        CancelInvoke("Explotion");
    }
}
