using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int hp;
    private BombEnemy bombEnemy;
    private Enemy enemy;

    
    public AK.Wwise.Event ouch;
    public AK.Wwise.Event death;
    void Start()
    {
        bombEnemy = GetComponent<BombEnemy>();
        enemy = GetComponent<Enemy>();
    }

    void Update()
    {
        
    }

    public void receiveDamage()
    {
        hp = hp - 1;
        ouch.Post(gameObject);
        if (hp <= 0)
        {
            if (bombEnemy != null)
                bombEnemy.Explotion();
            this.Death();
        }
        if (enemy != null)
        {
            enemy.StartCoroutine("Stop");
        }
    }

    private void Death()
    {
        death.Post(gameObject);
        Destroy(gameObject);
    }

}
