using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProyectile : MonoBehaviour
{
    public float speed;
    private GameObject player;
    public AK.Wwise.Event magic, stopMagic, magicEx, eco;

    void Start()
    {
        player = GameObject.Find("Player");
        magic.Post(gameObject);
        Invoke("DestroyPro", 5);
        eco.Post(gameObject);
    }


    void Update()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, player.transform.position, speed * Time.deltaTime);
        
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            DestroyPro();
            magicEx.Post(gameObject);
        }

    }
    private void DestroyPro()
    {
        stopMagic.Post(gameObject);
        Destroy(this.gameObject, 0.1f);
    }
}
