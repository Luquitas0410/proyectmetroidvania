using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicianEnemy : MonoBehaviour
{
    public float nextAttackTime, betweenAttacksTime;
    private Rigidbody2D rb;
    private Animator Animator;

    private GameObject player;
   [SerializeField] private float distDetection;

   
    public GameObject Pro1, Pro2, ProPoint;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();
        player = GameObject.Find("Player"); 
    }

    // Update is called once per frame
    void Update()
    {
     
        Vector3 distance = player.transform.position - this.transform.position;

        if (distance.magnitude <= distDetection)
        {
            if (nextAttackTime > 0)
            {
                nextAttackTime -= Time.deltaTime;
            }
            if (nextAttackTime <= 0)
            {
                Animator.SetTrigger("Attack");
                Invoke("LaunchPro", 0.5f);
                nextAttackTime = betweenAttacksTime;
            }
            
        } 
    }

    void LaunchPro()
    {
       
        int x = Random.Range(0, 2);

        if (x == 1)
            Instantiate(Pro1, ProPoint.transform.position, Quaternion.identity);
        if (x == 0)
            Instantiate(Pro2, ProPoint.transform.position, Quaternion.identity);
        CancelInvoke("LaunchPro");
    }
    private void Following()
    {
        if (transform.position.x < player.transform.position.x)
            transform.localScale = new Vector3(1f, 1f, 1f);
        else
            transform.localScale = new Vector3(-1f, 1f, 1f);
    }

   /* private void OnDrawGizmos()
   {
       Gizmos.color = Color.red;
       Gizmos.DrawWireSphere(this.transform.position, distDetection);
   }*/
}
