using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HangedEnemy : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator Animator;

    public float speed;
    private GameObject player;
    public float distDetection;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();
        player = GameObject.Find("Player");
        rb.gravityScale = 0;
       
    }

    void Update()
    {
        Animator.SetBool("Iddle", true);
        Vector3 distance = player.transform.position - this.transform.position;

        if (distance.magnitude <= distDetection)
        {
            Animator.SetBool("Spawn", true);
            Invoke("Following", 1f);
            distDetection = 8;
        }
        else
            Animator.SetBool("Running", false);

    }
    
    private void Following()
    {
        rb.gravityScale = 1;
        this.transform.position = Vector3.MoveTowards(this.transform.position, player.transform.position, speed * Time.deltaTime);
        Animator.SetBool("Running", true);

        if (transform.position.x < player.transform.position.x)
            transform.localScale = new Vector3(1f, 1f, 1f);
        else
            transform.localScale = new Vector3(-1f, 1f, 1f);
    }
}
