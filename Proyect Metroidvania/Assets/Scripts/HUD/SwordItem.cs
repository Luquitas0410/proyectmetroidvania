using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class SwordItem : MonoBehaviour
{
    private GameObject player;
    public GameObject buttom;
    public GameObject HUDimages;
    PlayerAtack playerAtack;

    public AK.Wwise.Event item;
    public AK.Wwise.Event mute;
    public AK.Wwise.Event unMute;
    void Start()
    {
        player = GameObject.Find("Player");
        buttom.SetActive(false);
        HUDimages.SetActive(false);

        playerAtack = player.GetComponent<PlayerAtack>();
    }

    void Update()
    {
       
            Vector3 distance = player.transform.position - this.transform.position;

            if (distance.magnitude <= 2)
            {
            if (Input.GetKeyDown("e"))
            {
                buttom.SetActive(false);
                if (playerAtack != null)
                {
                    if (HUDimages != null)
                    {
                        HUDimages.SetActive(true);
                    }
                    BetweenScenesController.haveSword = true;
                    playerAtack.enabled = true;
                }
                item.Post(gameObject);
                mute.Post(gameObject);
                unMute.Post(gameObject);
                Destroy(this.gameObject);
                }
                else
                    buttom.SetActive(true);
            }
            else
                buttom.SetActive(false);


        
    }

}

