using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement instance;
    private Rigidbody2D rb2D;
    private Animator Animator;
    //movement
    public float Speed, JumpForce, TrampJumpForce;
    private float Horizontal, Vertical, inputX;

    //playerAtack
    PlayerAtack playerAtack;
    //dash
    private Dash dash;
    public float horizontal => Horizontal;
    public float vertical => Vertical;

    //jump
    public bool Grounded;
    // knockback
    public bool canMove = true;
    public Vector2 bounceSpeed;

    // wall slide
    [SerializeField]private Transform wallController;
    [SerializeField] private Vector3 wallDimentions;
    [SerializeField] private bool inTheWall;
    [SerializeField] private bool sliding;
    [SerializeField] private float slideSpeed;
    [SerializeField] private LayerMask whatIsPlatform;

    public AK.Wwise.Event jump;

    private void Awake()
    {
    
    }
    void Start()
    {
        dash = GetComponent<Dash>();
        playerAtack = GetComponent<PlayerAtack>();
        dash.enabled = false;
        playerAtack.enabled = false;
        rb2D = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();
    }
    void Update()
    {
        if (!dash.IsDashing)
        {
            Jump();
        }

        if (Physics2D.Raycast(transform.position, Vector3.down, 1.1f))
            Grounded = true;
        else
            Grounded = false;

        if (!Grounded && inTheWall && inputX != 0)
            sliding = true;
        else
            sliding = false;

       
    }
    private void FixedUpdate()
    {
        if (!dash.IsDashing && canMove)
            Movement();

        inTheWall = Physics2D.OverlapBox(wallController.position, wallDimentions, 0f, whatIsPlatform);

        if (sliding)
        {
            rb2D.AddForce(Vector2.down * slideSpeed);
        }
    }
    private void Movement()
    {
        inputX = Input.GetAxisRaw("Horizontal");

        Horizontal = inputX;
         Vertical = Input.GetAxisRaw("Vertical");

        rb2D.velocity = new Vector2(Horizontal * Speed, rb2D.velocity.y);

        if (Horizontal < 0.0f)
            transform.localScale = new Vector3(-1f, 1f, 1f);
        else if (Horizontal > 0.0f)
            transform.localScale = new Vector3(1f, 1f, 1f);

        Animator.SetBool("Running", Horizontal != 0.0f);
    }
    private void Jump()
    {
      
        if (Input.GetButtonDown("Jump") && Grounded)
        {
            jump.Post(gameObject);
            rb2D.AddForce(Vector2.up * JumpForce);
        }
    }
    public void Rebote(Vector2 punchPoint)
    {
        rb2D.velocity = new Vector2(-bounceSpeed.x, bounceSpeed.y * punchPoint.y);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(wallController.position, wallDimentions);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Trampoline"))
            rb2D.AddForce(Vector2.up * TrampJumpForce);
    }

 
}
