using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAtack : MonoBehaviour
{

    public float radioHit, damageHit, nextAttackTime, betweenAttacksTime;
    public Transform atackPoint;
    private Animator Animator;
    public GameObject blood;

    public AK.Wwise.Event attack;

    void Start()
    {
        Animator = GetComponent<Animator>();    
    }

    // Update is called once per frame
    void Update()
    {
        Attack();
    }
    private void Attack()
    {

        if (nextAttackTime > 0)
        {
            nextAttackTime -= Time.deltaTime;
        }
        if (Input.GetButtonDown("Fire1") && nextAttackTime <= 0)
        {
            attack.Post(gameObject);
            Hit();
            nextAttackTime = betweenAttacksTime;
        }
    }

    private void Hit()
    {
        Animator.SetTrigger("Attack");
        Collider2D[] objects = Physics2D.OverlapCircleAll(atackPoint.position, radioHit);

        foreach (Collider2D collisioner in objects)
        {
            if (collisioner.CompareTag("Enemy"))
            {
                collisioner.transform.GetComponent<EnemyHealth>().receiveDamage();
                Instantiate(blood, collisioner.transform.position, Quaternion.identity);    
            }

        }
    }
   /* private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(atackPoint.position, radioHit);
    }*/
}
