using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPoint : MonoBehaviour
{
    private GameObject player;
    public GameObject buttom;
    public Health health;

   
    void Start()
    {
        player = GameObject.Find("Player");
    
        buttom.SetActive(false);
    }

 
    void Update()
    {
       
        Vector3 distance = player.transform.position - this.transform.position;

        if (distance.magnitude <= 1.7f)
        {
            if (Input.GetKeyDown("e"))
            {
                buttom.SetActive(false);
                bool hpRecuperate = health.AddHP();
                

                if (hpRecuperate)
                Destroy(this.gameObject);
            }
            else
                buttom.SetActive(true);
        }
        else
            buttom.SetActive(false);
    }
}
