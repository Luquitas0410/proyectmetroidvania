using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Windows;

public class Health : MonoBehaviour
{
    private GameObject[] hpoints;

    public int hp = 5;

    private string hpPrefs = "hp";
    public HUD hud;
    public GameObject eraser;
    public GameObject pointReference;
    public Transform parent;
    private Animator Animator;

    // knockback
    private PlayerMovement playerMovement;
    public float loseControlTime;

    public AK.Wwise.Event hpup, stopMusic, ouch, death;
   
    private void Awake()
    {
        LoadHP();
    }
    void Start()
    {
        hpoints = new GameObject[5];
        Animator = GetComponent<Animator>();
        playerMovement = GetComponent<PlayerMovement>();
        hpoints[0] = GameObject.Find("Point");
        hpoints[1] = GameObject.Find("Point (1)");
        hpoints[2] = GameObject.Find("Point (2)");
        hpoints[3] = GameObject.Find("Point (3)");
        hpoints[4] = GameObject.Find("Point (4)");
    }

    void Update()
    {
        if (hp == 5)
        {
            hpoints[0].SetActive(true);
            hpoints[1].SetActive(true);
            hpoints[2].SetActive(true);
            hpoints[3].SetActive(true);
            hpoints[4].SetActive(true);
        }
        else if (hp == 4)
        {
            hpoints[0].SetActive(true);
            hpoints[1].SetActive(true);
            hpoints[2].SetActive(true);
            hpoints[3].SetActive(true);
            hpoints[4].SetActive(false);
        }
        else if (hp == 3)
        {
            hpoints[0].SetActive(true);
            hpoints[1].SetActive(true);
            hpoints[2].SetActive(true);
            hpoints[3].SetActive(false);
            hpoints[4].SetActive(false);
        }
        else if (hp == 2)
        {
            hpoints[0].SetActive(true);
            hpoints[1].SetActive(true);
            hpoints[2].SetActive(false);
            hpoints[3].SetActive(false);
            hpoints[4].SetActive(false);
        }
        else if (hp == 1)
        {
            hpoints[0].SetActive(true);
            hpoints[1].SetActive(false);
            hpoints[2].SetActive(false);
            hpoints[3].SetActive(false);
            hpoints[4].SetActive(false);
        }
        else if (hp == 0)
        {
            hpoints[0].SetActive(false);
            hpoints[1].SetActive(false);
            hpoints[2].SetActive(false);
            hpoints[3].SetActive(false);
            hpoints[4].SetActive(false);
        }
    }

   
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy") && hp > 0)
        {
            ReceiveDamage(other.GetContact(0).normal);
        }
    }
    private void SceneRestart()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name);
            
        playerMovement.enabled = true;
        GameDataController.Instance.LoadData();
        hp = 5;
    }
    public void Death()
    {
        stopMusic.Post(gameObject);
        death.Post(gameObject);
        Animator.SetTrigger("Death");
        playerMovement.enabled = false;
        Invoke("SceneRestart", 2f);
        BetweenScenesController.Instance.AddDeath();
    }
    public void ReceiveExplotionDamage()
    {
        hp = hp - 2;
        Debug.Log("Perdiste 2 punto de vida");

    }
    public bool AddHP()
    {
        if (hp == 5)
            return false;

            hpup.Post(gameObject);
            hp += 1;
            return true;
    }


    public void ReceiveDamage(Vector2 position)
    {
        ouch.Post(gameObject);
        Animator.SetTrigger("Damage");
        hp -= 1;
        Instantiate(eraser, pointReference.transform.position, Quaternion.identity, parent.transform);

        if (hp <= 0)
        {
            Death();
        }
        StartCoroutine(LoseControl());
        StartCoroutine(DesactivateCollision());
        playerMovement.Rebote(position);
    }
    private IEnumerator LoseControl()
    {
        playerMovement.canMove = false;
        yield return new WaitForSeconds(loseControlTime);
        playerMovement.canMove = true;
    }
    private IEnumerator DesactivateCollision()
    {
        Physics2D.IgnoreLayerCollision(7, 8, true);
        yield return new WaitForSeconds(loseControlTime);
        Physics2D.IgnoreLayerCollision(7, 8, false);
    }

    private void OnDestroy()
    {
        SaveHP();
    }
    public void SaveHP()
    {
        PlayerPrefs.SetInt(hpPrefs, hp);
    }
    public void LoadHP()
    {
        hp = PlayerPrefs.GetInt(hpPrefs);
    }


}
