using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class Dash : MonoBehaviour
{
    public PlayerMovement scriptMovement;

    // private Map myInput;
    private Rigidbody2D rb;
    private PlayerMovement player;
    private float baseGravity;

    private Animator Animator;

    [Header("Dash")]
    [SerializeField] private float dashingTime = 0.2f;
    [SerializeField] private float dashForce = 20f;
    [SerializeField] private float dashForceVer = 10f;
    [SerializeField] private float timeCanDash = 3f;
    private bool isDashing;

    public bool IsDashing => isDashing;
    public bool canDash = true;

 /*   //cooldown
    public Image coolDown;
    public float current;
    public float max;*/

    public GameObject JPactive;
    public GameObject JPdesactive;

    public AK.Wwise.Event fire;

    void Start()
    {
        //  myInput = new Map();
        //  myInput.Movement.Enable();
        Animator = GetComponent<Animator>();
     //   JPactive.SetActive(false);
      //  JPdesactive.SetActive(false);
    }
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GetComponent<PlayerMovement>();
        baseGravity = rb.gravityScale;
    }


    void Update()
    {
      //  GetCurrentFill();
        if (Input.GetKeyDown("space"))
        {
            StartCoroutine(DashHor());
            StartCoroutine(DashVer());

            Animator.SetBool("Dashing", true);
        }
        else
        {
            Animator.SetBool("Dashing", false);
        }
/*
        if (current < max)
        {
          current += Time.deltaTime * 35f;
            JPdesactive.SetActive(true);
            JPactive.SetActive(false);
        }
        else 
        {
            JPactive.SetActive(true);
            JPdesactive.SetActive(false);
        }*/
    }
    private IEnumerator DashHor()
    {

        if (player.horizontal != 0 && canDash)
        {
            fire.Post(gameObject);
           // current = 0;
            isDashing = true;
            canDash = false;
            //  rb.gravityScale = 0f;
            rb.velocity = new Vector2(player.horizontal * dashForce, 0f);
            yield return new WaitForSeconds(dashingTime);
            
            isDashing = false;
            yield return new WaitForSeconds(timeCanDash);

            canDash = true;

        }
    }

    private IEnumerator DashVer()
    {
        if (player.vertical != 0 && canDash)
        {
            fire.Post(gameObject);
         //   current = 0;
            isDashing = true;
            canDash = false;
            // rb.gravityScale = 0f;
            rb.velocity = new Vector2(0f, player.vertical * dashForceVer);
            yield return new WaitForSeconds(dashingTime);
            
             isDashing = false;
            yield return new WaitForSeconds(timeCanDash);

            canDash = true;
        }
    }

  /* void GetCurrentFill()
    {
        float fillAmount = current / max;
        coolDown.fillAmount = fillAmount;
    }
   */

}
